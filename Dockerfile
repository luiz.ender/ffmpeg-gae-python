FROM python:3.9
COPY ./src/app/requirements.txt /build/requirements.txt
RUN apt-get update && \
    apt-get install -q -y ffmpeg && \
    pip install -r /build/requirements.txt --no-cache-dir
ADD . /code/
WORKDIR /code/
ENV PYTHONPATH="/code/src:${PYTHONPATH}"
CMD exec gunicorn --bind :${PORT:-8000} --workers 1 --log-level ${LOG_LEVEL:-info} --timeout ${WORKER_TIMEOUT:-300} --reload app.main:APP