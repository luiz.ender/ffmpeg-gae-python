"""
This module has the Flask App

"""

import os
import re
import logging
import time
import datetime
import ffmpeg
from flask import Flask, jsonify, request, abort
from google.cloud.storage import Client, Blob


APP = Flask(__name__)

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    APP.logger.handlers = gunicorn_logger.handlers
    APP.logger.setLevel(gunicorn_logger.level)


def _parser_uri(uri):
    """Parser the URI and return bucket and filename

    :param uri: The URI to parser
    :type uri: str
    :return: The tuple with bucket and filename
    :rtype: tuple
    """
    match = re.match(r'^gs://(?P<bucket>.+?)/(?P<filename>.+)$', uri)
    if not match:
        return None, None
    return match.group('bucket'), match.group('filename')


def _input_signed_url(uri, storage):
    """Generate the signed url to input file

    :param uri: The URI of input file
    :type uri: str
    :param storage: The storage client
    :type storage: google.cloud.storage.Client
    :return: The signed url of input file
    :rtype: str
    """
    input_blob = Blob.from_string(uri, client=storage)
    if not input_blob.exists():
        abort(400, jsonify(message='The blob %s does not exists' % uri))

    APP.logger.debug('Generating signed url of input file %s', input_blob.name)
    return input_blob.generate_signed_url(
        expiration=datetime.datetime.now() + datetime.timedelta(hours=1), version='v4'
    )


@APP.route('/transcode', methods=['POST'])
def transcode():
    """Transcode the Video

    :return: The data of video transcoded
    :rtype: flask.Response
    """
    start_time = time.time()
    data = request.get_json()

    storage = Client()
    bucket, input_filename = _parser_uri(data.get('uri'))
    output_name, _ = os.path.splitext(os.path.basename(input_filename))
    output_local = '/tmp/%s-%d.mp4' % (output_name, datetime.datetime.now().timestamp())

    main = ffmpeg.input(_input_signed_url(data.get('uri'), storage))
    watermark = ffmpeg.input('%s/logo.png' % os.path.dirname(__file__))
    main_scale = ffmpeg.filter(main.video, 'scale', '-2', '360')
    watermark_scale = ffmpeg.filter_multi_output(
        [watermark, main_scale], 'scale2ref', w='oh*mdar', h='ih*0.2'
    )
    watermark_transparent = ffmpeg.colorchannelmixer(watermark_scale[0], aa=0.5)
    overlay = ffmpeg.filter(
        [watermark_scale[1], watermark_transparent],
        'overlay', '(main_w-overlay_w)/2', '(main_h-overlay_h)/2'
    )
    output = (
        ffmpeg
        .concat(overlay, main.audio, v=1, a=1)
        .output(output_local, format='mp4', preset='faster', tune='zerolatency', **{
            'c:a': 'aac', 'strict': 'experimental', 'b:a': '64k', 'ar': '44100', 'ac': 1,
            'c:v': 'libx264', 'b:v': '1M', 'maxrate': '1M', 'bufsize': '1M'
        })
    )

    APP.logger.debug('ffmpeg %s', ' '.join(output.get_args(overwrite_output=True)))
    output.run(overwrite_output=True)

    output_uri = 'gs://%s/transcode/%s' % (bucket, os.path.basename(output_local))
    output_blob = Blob.from_string(output_uri, client=storage)
    APP.logger.debug('Uploading local file %s to %s', output_local, output_uri)
    output_blob.upload_from_filename(output_local, content_type='video/mp4')

    APP.logger.debug('Elaped time: %s', time.time()-start_time)

    return jsonify(
        uri=output_uri, size=output_blob.size, content_type=output_blob.content_type
    )


@APP.route('/thumbnail', methods=['POST'])
def thumbnail():
    """Creating the thumbnail of Video

    :return: The data of thumbail
    :rtype: flask.Response
    """
    start_time = time.time()
    data = request.get_json()

    storage = Client()
    bucket, input_filename = _parser_uri(data.get('uri'))
    output_name, _ = os.path.splitext(os.path.basename(input_filename))
    output_local = '/tmp/%s-%d.png' % (output_name, datetime.datetime.now().timestamp())

    main = ffmpeg.input(_input_signed_url(data.get('uri'), storage))
    output = main.video.output(output_local, vf='thumbnail', vframes=1)

    APP.logger.debug('ffmpeg %s', ' '.join(output.get_args(overwrite_output=True)))
    output.run(overwrite_output=True)

    output_uri = 'gs://%s/thumbnail/%s' % (bucket, os.path.basename(output_local))
    output_blob = Blob.from_string(output_uri, client=storage)
    APP.logger.debug('Uploading local file %s to %s', output_local, output_uri)
    output_blob.upload_from_filename(output_local, content_type='image/png')

    APP.logger.debug('Elaped time: %s', time.time()-start_time)

    return jsonify(uri=output_uri, size=output_blob.size, content_type=output_blob.content_type)


@APP.route('/audio', methods=['POST'])
def audio():
    """Extract audio from Video

    :return: The data of audio
    :rtype: flask.Response
    """
    start_time = time.time()
    data = request.get_json()

    storage = Client()
    bucket, input_filename = _parser_uri(data.get('uri'))
    output_name, _ = os.path.splitext(os.path.basename(input_filename))
    output_local = '/tmp/%s-%d.flac' % (output_name, datetime.datetime.now().timestamp())

    output = (
        ffmpeg
        .input(_input_signed_url(data.get('uri'), storage))
        .output(
            output_local, format='flac', acodec='flac', ac=1, ar=44100, audio_bitrate='64k',
            vn=None, sn=None, dn=None
        )
    )

    APP.logger.debug('ffmpeg %s', ' '.join(output.get_args(overwrite_output=True)))
    output.run(overwrite_output=True)

    output_uri = 'gs://%s/audio/%s' % (bucket, os.path.basename(output_local))
    output_blob = Blob.from_string(output_uri, client=storage)
    APP.logger.debug('Uploading local file %s to %s', output_local, output_uri)
    output_blob.upload_from_filename(output_local, content_type='audio/flac')

    APP.logger.debug('Elaped time: %s', time.time()-start_time)

    return jsonify(uri=output_uri, size=output_blob.size, content_type=output_blob.content_type)


@APP.route('/trim', methods=['POST'])
def trim():
    """Trim from Video

    :return: The data of video
    :rtype: flask.Response
    """
    start_time = time.time()
    data = request.get_json()

    storage = Client()
    bucket, input_filename = _parser_uri(data.get('uri'))
    output_name, extension = os.path.splitext(os.path.basename(input_filename))
    output_local = '/tmp/%s-%d%s' % (output_name, datetime.datetime.now().timestamp(), extension)

    main = ffmpeg.input(_input_signed_url(data.get('uri'), storage))
    output = ffmpeg.output(
        main.video, main.audio, output_local,
        ss=data.get('start'), to=data.get('end'), c='copy'
    )

    APP.logger.debug('ffmpeg %s', ' '.join(output.get_args(overwrite_output=True)))
    output.run(overwrite_output=True)

    output_uri = 'gs://%s/trim/%s' % (bucket, os.path.basename(output_local))
    output_blob = Blob.from_string(output_uri, client=storage)
    APP.logger.debug('Uploading local file %s to %s', output_local, output_uri)
    output_blob.upload_from_filename(output_local)

    APP.logger.debug('Elaped time: %s', time.time()-start_time)

    return jsonify(uri=output_uri, size=output_blob.size, content_type=output_blob.content_type)


@APP.route('/health', methods=['GET'])
def health():
    """Get the status of App

    :return: The status of App
    :rtype: str
    """
    return 'App is running'


if __name__ == '__main__':
    APP.run(debug=True, host='0.0.0.0', port=8000)
